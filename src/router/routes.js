import Layout from "@/Layout";

/**
 * @title: 路由标题
 * @hidden: 是否显示该菜单
 * @isFooter: 是否需要底部导航
 * @crumbs: 是否需要面包屑
 * @routeLazy: 是否需要路由懒加载
 */
export default [
  {
    path: "/",
    name: "layout",
    component: Layout,
    redirect: "home",
    meta: {
      title: "layout",
      hidden: false,
    },
    children: [
      {
        path: "home",
        name: "home",
        component: () => {
          return import("@/views/home");
        },
        meta: {
          title: "首页",
          hidden: true,
          isFooter: true,
        },
      },
    ],
  },
];
