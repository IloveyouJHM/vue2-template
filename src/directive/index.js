import Vue from "vue";

/**
 * 解决radioGroup的aria-hidden问题
 *
 * @returns 无返回值
 */
export function solveRadioGroup() {
  Vue.directive("removeAriaHidden", {
    bind(el, binding) {
      let ariaEls = el.querySelectorAll(".el-radio__original");
      ariaEls.forEach((item) => {
        item.removeAttribute("aria-hidden");
      });
    },
  });
}
