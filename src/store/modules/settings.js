import defaultSettings from "@/settings";
const { title } = defaultSettings;
const state = {
  title: title || "",
};
const mutations = {};

const actions = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
