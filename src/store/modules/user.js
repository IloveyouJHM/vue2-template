import { getToken, getUser } from "@/utils/auth";
const state = {
  token: getToken() || "",
  userInfo: getUser() || {},
};
const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_USERINFO: (state, userInfo) => {
    state.userInfo = userInfo;
  },
  CLEAR_USERINFO: (state) => {
    state.userInfo = {};
    state.token = "";
  },
};

const actions = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
