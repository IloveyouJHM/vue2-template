import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/styles/variables.css";
import "amfe-flexible";
import globalMethods from "@/utils/globalMethods";

Vue.config.productionTip = false;

Vue.use(globalMethods);
new Vue({
  router,
  store,
  render: function (h) {
    return h(App);
  },
}).$mount("#app");
