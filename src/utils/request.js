import axios from "axios";
import store from "@/store";
import handleError from "./message";
import nProgress from "nprogress";
import "nprogress/nprogress.css";
import { getToken } from "./auth";

const requestTimeOut = 5 * 1000;
const success = 200;

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: requestTimeOut,
  responseType: "json",
  validateStatus(status) {
    return status === success;
  },
});

// 请求拦截器
service.interceptors.request.use(
  (config) => {
    nProgress.start();
    const _config = config;
    try {
      let token = store.state.user.token || getToken() || "";

      if (token) {
        _config.headers["Authorization"] = "Bearer" + token;
      }
    } catch (e) {
      console.error(e);
    }
    return _config;
  },
  (error) => {
    nProgress.done();
    return Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (res) => {
    nProgress.done();
    if (
      res.config.responseType === "blob" ||
      res.config.responseType === "arraybuffer"
    ) {
      return res.data.data;
    }

    if (res.status !== success || res.data.code !== success) {
      handleError(`请求错误：${res.data.msg}`);
      return Promise.reject(new Error(res.data.msg));
    } else {
      return res.data.data;
    }
  },
  (error) => {
    let errMsg = error.message || "网络异常，请稍后再试";
    let status = error.status;
    nProgress.done();

    switch (status) {
      case 401:
        handleError(errMsg);
      case 403:
        handleError(errMsg);
        break;
      case 404:
        handleError(errMsg);
        break;
      case 500:
        handleError(errMsg);
        break;
      default:
        handleError(errMsg);
    }

    return Promise.reject(new Error(errMsg));
  }
);

export function get(url, params = {}, config = {}) {
  return service.get(url, { params, ...config });
}

export function post(url, data = {}, config = {}) {
  return service.post(url, data, config);
}

export function put(url, data = {}, config = {}) {
  return service.put(url, data, config);
}

export function patch(url, data = {}, config = {}) {
  return service.patch(url, data, config);
}

export function remove(url, params = {}, config = {}) {
  return service.delete(url, { params, ...config });
}

export function all(requestFunctions) {
  return Promise.all(requestFunctions.map((func) => func()));
}

export function exportExcel(
  url,
  params = {},
  config = { responseType: "blob" }
) {
  return service.get(url, { params, ...config }).then((response) => {
    const blob = new Blob([response.data], {
      type: "application/vnd.ms-excel",
    });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = "exported_excel.xlsx";
    link.click();
    URL.revokeObjectURL(link.href);
  });
}

export function exportZip(url, params = {}, config = { responseType: "blob" }) {
  return service.get(url, { params, ...config }).then((response) => {
    const blob = new Blob([response.data], { type: "application/zip" });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = "exported_zip.zip";
    link.click();
    URL.revokeObjectURL(link.href);
  });
}
