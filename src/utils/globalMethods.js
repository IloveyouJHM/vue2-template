import { solveRadioGroup } from "@/directive";
// import {} from "@/components";

const components = {};

export default {
  install(Vue) {
    Object.keys(components).forEach((key) => {
      Vue.component(key, components[key]);
    });

    solveRadioGroup();

    Vue.mixin(routeMetaMixin);
  },
};
