export function setToken(token) {
  return localStorage.setItem("ACCESS_TOKEN", token);
}

export function getToken() {
  return localStorage.getItem("ACCESS_TOKEN");
}

export function removeToken() {
  return localStorage.removeItem("ACCESS_TOKEN");
}

export function setUser(user) {
  return localStorage.setItem("USER_INFO", JSON.stringify(user));
}

export function getUser() {
  return localStorage.getItem("USER_INFO");
}

export function removeUser() {
  return localStorage.removeItem("USER_INFO");
}

export function clearLocalStorage() {
  localStorage.clear();
}
