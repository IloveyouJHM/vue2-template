import defaultSettings from "@/settings";

const title = defaultSettings.title;

export default function getPageTitle(pageTitle, pagePath) {
  if (pageTitle) {
    if (pagePath == "/home") {
      return title;
    } else {
      return `${pageTitle} - ${title}`;
    }
  }
  return `${title}`;
}
