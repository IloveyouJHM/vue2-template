/**
 * 获取格式化后的当前时间
 *
 * @returns 返回格式为 "yyyy-MM-dd HH:mm:ss" 的字符串
 */
export function getFormattedTime() {
  const now = new Date();
  const year = now.getFullYear();
  const month = String(now.getMonth() + 1).padStart(2, "0");
  const day = String(now.getDate()).padStart(2, "0");
  const hours = String(now.getHours()).padStart(2, "0");
  const minutes = String(now.getMinutes()).padStart(2, "0");
  const seconds = String(now.getSeconds()).padStart(2, "0");
  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

/**
 * 重置表单数据
 *
 * @param formData 表单数据对象
 * @returns 重置后的表单数据对象
 */
export function resetFormData(formData) {
  const keys = Object.keys(formData);
  keys.forEach((key) => {
    formData[key] = "";
  });
  return formData;
}
