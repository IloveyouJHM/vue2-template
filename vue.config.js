const { defineConfig } = require("@vue/cli-service");
const { title } = require("./src/settings");

module.exports = defineConfig({
  // 开启ESLint校验
  lintOnSave: true,
  transpileDependencies: true,
  devServer: {
    allowedHosts: [".natappfree.cc"],
    client: {
      overlay: false,
    },
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: `http://ylbparks.gpoj.cnitcs.net/prod-api`,
        changeOrigin: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_BASE_API]: "",
        },
      },
    },
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
         @use "~@/styles/index.scss" as *;
        `,
      },
      postcss: {
        postcssOptions: {
          plugins: [
            require("postcss-pxtorem")({
              rootValue: 192,
              propList: ["*"],
              selectorBlackList: [".noConvert"],
            }),
          ],
        },
      },
    },
  },
  configureWebpack: {
    plugins: [
      new (require("webpack").DefinePlugin)({
        APP_TITLE: JSON.stringify(title),
      }),
    ],
  },
});
