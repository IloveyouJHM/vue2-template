module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    "plugin:vue/recommended", // 使用 Vue 插件提供的规则
    "standard", // 使用 Standard 风格的规则
  ],
  rules: {
    // 开发环境禁用 console 和 debugger
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    // 强制使用分号结尾
    semi: ["error", "always"],
    // 强制使用 2 个空格缩进
    indent: ["error", 2],
    // 禁止未使用的变量
    "no-unused-vars": [
      "error",
      { vars: "all", args: "after-used", ignoreRestSiblings: false },
    ],
    // 禁止空语句
    "no-empty": "error",
    // 禁止使用 var 关键字
    "no-var": "error",
    // 鼓励使用 const
    "prefer-const": [
      "error",
      { destructuring: "any", ignoreReadBeforeAssign: false },
    ],
    // 箭头函数参数括号风格
    "arrow-parens": ["error", "as-needed"],
    // 对象大括号前后空格
    "object-curly-spacing": ["error", "always"],
    // 圆括号内部空格
    "space-in-parens": ["error", "never"],
    // 二元运算符前后空格
    "space-infix-ops": "error",
    // 对象键和冒号之间的空格
    "key-spacing": ["error", { beforeColon: false, afterColon: true }],
    // 对象或数组的最后一个元素之后的逗号
    "comma-dangle": ["error", "never"],
    // 函数表达式名称
    "func-names": ["warn", "never"],
    // 一致的返回语句
    "consistent-return": "error",
    // 最大行长度
    "max-len": [
      "error",
      {
        code: 120,
        ignoreUrls: true,
        ignoreComments: false,
        ignoreTrailingComments: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreRegExp: true,
      },
    ],
    // 禁止行尾空格
    "no-trailing-spaces": "error",
    // 空行的最大数量
    "no-multiple-empty-lines": ["error", { max: 1, maxEOF: 1, maxBOF: 0 }],
    // 语句之间空行
    "padding-line-between-statements": [
      "error",
      { statements: ["const", "let", "var"], next: "*", blankLine: "always" },
      {
        statements: ["class", "return", "throw", "^export"],
        next: "*",
        blankLine: "always",
      },
    ],
    // 禁止字符串中的模板字符串语法
    "no-template-curly-in-string": "error",
    // 禁止不必要的字符串连接
    "no-useless-concat": "error",
    // 禁止某些语法结构
    "no-restricted-syntax": [
      "error",
      "ForInStatement",
      "LabeledStatement",
      "WithStatement",
    ],
    // 禁止重新赋值函数参数
    "no-param-reassign": ["error", { props: false }],
    // 禁止 else 语句后面的 return 语句
    "no-else-return": "warn",
    // 鼓励使用箭头函数
    "prefer-arrow-callback": "warn",
    // 鼓励使用模板字符串
    "prefer-template": "warn",
    // 禁止常量条件
    "no-constant-condition": "error",
    // 禁止不必要的三元操作符
    "no-unneeded-ternary": "warn",
    // 禁止隐式类型转换
    "no-implicit-coercion": "warn",
    // 禁止循环体内的函数声明
    "no-loop-func": "warn",
    // 禁止某些全局变量
    "no-restricted-globals": ["error", "isFinite", "isNaN"],
    // 禁止某些属性
    "no-restricted-properties": [
      "error",
      {
        object: "arguments",
        property: "callee",
        message: "arguments.callee is deprecated",
      },
      {
        object: "global",
        property: "isFinite",
        message: "Please use Number.isFinite instead",
      },
    ],
    // 禁止变量初始化为 undefined
    "no-undef-init": "error",
    // 禁止提前使用变量
    "no-use-before-define": ["error", "nofunc"],
    // 禁止使用 void 操作符
    "no-void": "error",
    // 禁止使用 with 语句
    "no-with": "error",
    // 鼓励使用解构赋值
    "prefer-destructuring": [
      "warn",
      { array: false, object: true },
      { enforceForRenamedProperties: false },
    ],
    // 禁止变量声明遮蔽外部变量
    "no-shadow": "warn",
    // 禁止遮蔽保留关键字
    "no-shadow-restricted-names": "error",
    // 禁止未使用的表达式
    "no-unused-expressions": "error",
    // 禁止多余的分号
    "no-extra-semi": "error",
    // 禁止使用 eval()
    "no-eval": "error",
    // 禁止使用隐式的 eval()
    "no-implied-eval": "error",
    // 禁止省略数字前后的 0
    "no-floating-decimal": "error",
    // 禁止单独的块
    "no-lone-blocks": "error",
    // 禁止 Function 构造函数的新实例
    "no-new-func": "error",
    // 禁止 String、Number、Boolean 新实例
    "no-new-wrappers": "error",
    // 禁止 Array 构造函数
    "no-array-constructor": "error",
    // 禁止 Object 构造函数的新实例
    "no-new-object": "error",
    // 禁止 Math、JSON 的调用
    "no-obj-calls": "error",
    // 禁止八进制字面量
    "no-octal": "error",
    // 禁止八进制转义序列
    "no-octal-escape": "error",
    // 禁止重复声明变量
    "no-redeclare": "error",
    // 禁止 script URL
    "no-script-url": "error",
    // 禁止逗号操作符
    "no-sequences": "error",
    // 禁止不必要的 .call() 和 .apply()
    "no-useless-call": "error",
    // 禁止不必要的计算属性键
    "no-useless-computed-key": "error",
    // 禁止不必要的构造函数
    "no-useless-constructor": "error",
    // 禁止不必要的重命名
    "no-useless-rename": "error",
    // 禁止不必要的 return 语句
    "no-useless-return": "error",
    // 禁止属性前的空格
    "no-whitespace-before-property": "error",
    // 异步函数必须有 await
    "require-await": "error",
    // generator 函数必须有 yield
    "require-yield": "error",
    // typeof 的有效性
    "valid-typeof": "error",
    // 禁止重复的键
    "no-dupe-keys": "error",
    // 禁止 switch 中重复的 case 标签
    "no-duplicate-case": "error",
    // 禁止稀疏数组
    "no-sparse-arrays": "error",
    // 禁止字符串中的模板字符串语法
    "no-template-curly-in-string": "error",
    // 禁止不可达代码
    "no-unreachable": "error",
    // 禁止未使用的标签
    "no-unused-labels": "error",
    // 禁止不必要的 break 语句
    "no-useless-break": "error",
    // 禁止不必要的转义字符
    "no-useless-escape": "error",
    // 禁止使用 var 关键字
    "no-var": "error",
    // typeof 的有效性
    "valid-typeof": "error",
  },
  parserOptions: {
    parser: "babel-eslint",
    sourceType: "module",
  },
};
